#include "fps_counter.hpp"
#include <cstdlib>
#include <iostream>
#include <unistd.h>

int main(int argc, char *argv[])
{
    // Test for the interval
    Interval i;
    i.reset();
    usleep(1000000);

    // Allow a margin of error of 500 microseconds
    if (abs(i.value() - 1000000) > 500)
    {
        std::cout << "Interval out of range by more than 500 microseconds" << std::endl;
        std::cout << "Interval read: " << i.value() << "us, Expected: 1000000" << std::endl;
        return -1;
    }

    // Test for the fps counter
    Fps_counter fps;
    for (int i = 0; i < 200; i++)
    {
        usleep(20 * 1000);
        fps.update();

        // Fps counter needs 1 second to gather information before
        // it can accurately detect the speed
        if (i > 50)
        {
            if (fps.get() != 50)
            {
                std::cout << "FPS counter out of range" << std::endl;
                std::cout << "FPS read: " << fps.get() << "fps, Expected: 50" << std::endl;
                return -2;
            }
        }
    }

    return 0;
}
