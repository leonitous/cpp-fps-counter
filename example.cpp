#include "fps_counter.hpp"
#include <iostream>
#include <unistd.h>

int main(int argc, char *argv[])
{
    Fps_counter fps;

    // Example processing loop
    for (int i = 0; i < 200; i++)
    {
        // Some really complex operation
        usleep(20 * 1000);

        fps.update();
        std::cout << "FPS is: " << fps.get() << std::endl;
    }
}
