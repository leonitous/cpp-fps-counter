# cpp fps counter

C++ library for calculating the fps/time for an event to run

## C++ Example

```cpp
#include "fps_counter.hpp"
#include <iostream>
#include <unistd.h>

int main(int argc, char *argv[])
{
    Fps_counter fps;

    // Example processing loop
    for (int i = 0; i < 200; i++)
    {
        // Some really complex operation
        usleep(20 * 1000);

        fps.update();
        std::cout << "FPS is: " << fps.get() << std::endl;
    }
}
```

### Compile Example File

```sh
g++ -g -Wall example.cpp -o example -std=c++11
./example
```

Example expected output should be FPS is: 50

### Compile Tests

```sh
g++ -g -Wall tests.cpp -o test -std=c++11
./test
```
